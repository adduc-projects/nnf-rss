module.exports = {
  entry: './resources/js/app.js',
  output: {
    filename: './public/bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader?importLoaders=1'
        ]
      },
      {
        test: /.scss$/,
        use: [
          'style-loader',
          'css-loader?importLoaders=1',
          'sass-loader'
        ]
      }
    ]
  }
}
