# RSS Feed Aggregator for Never Not Funny

Software for fetching and aggregating purchased Never Not Funny content
into a single feed for audio or video.

## Requirements

* PHP / Composer
* Node / Yarn

## Installation

```bash
composer install
yarn install
yarn run webpack

```

## License

This software is open-sourced software licensed under the
[MIT license](http://opensource.org/licenses/MIT).
