<?php

namespace App\Providers;

use App\User;
use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\TransferStats;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $func = function (Request $request) {
            $user = $request->getUser();
            $password = $request->getPassword();

            $cache = $this->app['cache'];
            $key = md5(json_encode([$user, $password]));

            $seasons = $cache->get($key, -1);

            if ($seasons === -1) {
                $guzzle = $this->app->make(Guzzle::class);
                $seasons = $this->fetchSeasons($guzzle, $user, $password);
                $cache->put($key, $seasons, 60);
            }

            // Convert false to null to allow auth guard to fail.
            return $seasons ?: null;
        };

        $this->app['auth']->viaRequest('api', $func);
    }

    /**
     * @param Request $request
     * @param string $user
     * @param string $password
     * @return array|false
     */
    protected function fetchSeasons(Guzzle $guzzle, $user, $password)
    {
        if (!$user || !$password) {
            return false;
        }

        $url = "https://pardcast.com/login.php";
        $data = [
            'username' => $user,
            'password' => $password,
        ];

        // Use a cookie jar to track logged-in session cookie.
        // Needed for seasons to show in response.
        $jar = new CookieJar();
        $result = $guzzle->post($url, [
            'cookies' => $jar,
            'form_params' => [
                'username' => $user,
                'password' => $password
            ],
            'on_stats' => function (TransferStats $stats) use (&$url) {
                $url = $stats->getEffectiveUri();
            }
        ]);

        if ($url->getPath() == '/members.php') {
            return false;
        }

        $seasons = [
            'audio' => [],
            'video' => [],
        ];

        preg_match_all(
            '#"(https://pardcast.com/feeds/[a-z]+[0-9]+(
                (?P<audio>/[a-z]+[0-9]+\.xml)
                |
                (?P<video>video/[a-z]+[0-9]+video\.xml)

            ))"#x',
            $result->getBody(),
            $matches
        );

        if (!$matches) {
            return $seasons;
        }

        foreach (array_filter($matches['audio']) as $key => $value) {
            $seasons['audio'][] = $matches[1][$key];
        }

        foreach (array_filter($matches['video']) as $key => $value) {
            $seasons['video'][] = $matches[1][$key];
        }

        return $seasons;
    }
}
