<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client as Guzzle;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Contracts\Cache\Repository as Cache;
use Laravel\Lumen\Routing\Controller as BaseController;
use SimpleXMLElement;

class Controller extends BaseController
{
    public function landing()
    {
        return view('landing');
    }

    /**
     * @param string $type
     * @param Request $request
     * @param Cache $cache
     * @param Guzzle $guzzle
     * @return Response
     */
    public function feed(string $type, Request $request, Cache $cache, Guzzle $guzzle)
    {
        $fetch = function() use ($type, $request, $cache, $guzzle) {
            $feed = $this->buildFeed($type, $request, $cache, $guzzle);
            return $feed;
        };

        $expires = 5; // 5 Minutes
        $feed = $cache->remember($request->getUser(), $expires, $fetch);
        $response = response($feed, 200, ['Content-Type' => 'application/xml']);
        return $response;
    }

    /**
     * @param string $type
     * @param Request $request
     * @param Cache $cache
     * @param Guzzle $guzzle
     * @return SimpleXMLElement
     */
    protected function buildFeed(string $type, Request $request, Cache $cache, Guzzle $guzzle)
    {
        $seasons = $request->user();
        foreach ($seasons[$type] as $id => $url) {
            $seasons[$type][$id] = $this->fetchSeason($url, $request, $cache, $guzzle);
        }
        $view = (string)view('feed', ['feeds' => $seasons[$type]]);
        return $view;
    }

    /**
     * @param string $url
     * @param Request $request
     * @param Cache $cache
     * @param Guzzle $guzzle
     * @return string
     */
    protected function fetchSeason(string $url, Request $request, Cache $cache, Guzzle $guzzle)
    {
        $fetch = function() use ($url, $request, $guzzle) {
            $user = $request->getUser();
            $password = $request->getPassword();

            $result = $guzzle->get($url, [
                'auth' => [$user, $password]
            ]);

            return (string)$result->getBody();
        };

        // We can't cache forever, because URLs for old episodes might
        // change (if they're moved), and because the latest season
        // may have new episodes added.
        $expires = 12 * 60; // 12 Hours
        $season = $cache->remember($url, $expires, $fetch);
        $season = new SimpleXMLElement($season);
        return $season;
    }
}
