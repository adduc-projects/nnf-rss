<?php

$add_child = function(&$xml1,&$xml2) {
    // Create new DOMElements from the two SimpleXMLElements
    $dom1 = dom_import_simplexml($xml1);
    $dom2 = dom_import_simplexml($xml2);
    // Import the  into the  document
    $dom2 = $dom1->ownerDocument->importNode($dom2, TRUE);
    // Append the  to 
    $dom1->appendChild($dom2);
};

$atom_ns = "http://www.w3.org/2005/Atom";

$str = '<?xml version="1.0" encoding="utf-8"?>'
     . '<rss version="2.0" xmlns:atom="' . $atom_ns . '"></rss>';

$xml = array_shift($feeds);
$xml->channel->title = "Never Not Funny Aggregate RSS Feed - UNOFFICIAL / FAN MADE";

$link = $xml->channel->xpath('atom:link')[0];
$node = dom_import_simplexml($link);
$node->parentNode->removeChild($node);

foreach ($feeds as $feed) {
    foreach ($feed->channel->item as $item) {        
        $add_child($xml->channel, $item);
    }
}

echo $xml->asXml();
