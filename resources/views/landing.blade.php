<!DOCTYPE html>
<html>
<head>
    <title>Unofficial Never Not Funny Feeds</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="./bundle.js"></script>
</head>
<body>
    <div class="content">
        <h1>Unofficial RSS Feeds for Never Not Funny</h1>

        <h2>Links</h2>
        <h3>Audio</h3>
        <p>https://&lt;username&gt;:&lt;password&gt;@nnf-rss.128.io/feed/audio</p>
        <h3>Video</h3>
        <p>https://&lt;username&gt;:&lt;password&gt;@nnf-rss.128.io/feed/video</p>

        <h2>Frequently Asked Questions</h2>
        <h3>What is this?</h3>
        <p>
            Never Not Funny doesn't provide an aggregate feed, where all
            purchased seasons / Pardcast-a-thons would be accessible.
            Instead, each season requires subscribing to a new feed.
            This site provides feeds for audio and video that aggregate
            together purchased seasons / Pardcast-a-thons together.
        </p>
        <h3>Why do I have to give you my username/password?</h3>
        <p>
            There's no other way to validate purchased content. While
            you're required to send credentials as part of every
            request, it is transmitted securely and never stored
            anywhere. In addition, the code is open source and available
            for review.
        </p>
    </div>
    <div class="footer">
        Made with ❤ by <a href="https://128.io">John Long</a>
        <br>
        <a href="https://paypal.me/adduc">Donations</a> |
        <a href="https://gitlab.128.io/my-projects/php/applications/nnf-rss-2">Source</a>
        <br>
        Hosted on <a href="http://www.vultr.com/?ref=7012465-3B">Vultr</a>
    </div>

</body>
</html>
